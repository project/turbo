<?php



/**
 * @file
 * provides memcache caching
 */

/**
* Connect to memcache server
* @return
*   TRUE
**/
function turbo_path_memcache_connect($bin) {
  if ( is_object($GLOBALS['turbo_path_memcache']) ) {
    return TRUE;
  }

  // create a global memcache module
  if (!extension_loaded('memcache') && !$_SESSION['turbo_path']['no_memcache']) {
    watchdog('turbo_path', 'Purbo Path memcache is used but no memcache extension is loaded', array(), 'error');
    $_SESSION['turbo_path']['no_memcache'] = TRUE; // to prevent numerous errors in watchdog
    return;
  }


  //drupal_set_message('creating a  new memcache object');
  $GLOBALS['turbo_path_memcache'] = new Memcache;

  if (sizeof($bin['servers']) == 1) {

    list($host, $port) = explode(":", $bin['servers'][0]);
    $GLOBALS['turbo_path_memcache'] -> pconnect($host, $port);

  }
  else {
    foreach ($bin['servers'] as $id => $server_info) {
      list ($host, $port) = explode(":", $server_info);
      $GLOBALS['turbo_path_memcache'] -> addServer($host, $port);
    }
  }
  return TRUE;

}

function turbo_path_memcache_lookup_path($action, $path = '', $path_language = '', $bin=NULL) {

  if ( !turbo_path_memcache_connect($bin) ) return;

  global $language;
  // $map is an array with language keys, holding arrays of Drupal paths to alias relations
  static $map = array(), $no_src = array(), $count;

  $path_language = $path_language ? $path_language : $language->language;

  // Use $count to avoid looking up paths in subsequent calls if there simply are no aliases
  $count=1;
  if ( !isset($count) ) {
    $count = turbo_path_memcache_count();
    if ($count<=0) $count = db_result(db_query('SELECT COUNT(pid) FROM {url_alias}'));
  }

  if ( $action == 'wipe' ) {
    $map = array();
    $no_src = array();
    $count = NULL;

    // it would be nice to do this but if that memcache instance is used to store something else it will be wiped too...

  }
  elseif ($count > 0 && $path != '') {

    if ($action == 'alias') {

      if (isset($map[$path_language][$path])) {
        return $map[$path_language][$path];
      }

      $alias=turbo_path_memcache_get('dst', $path, $path_language, $bin);

      if ($alias!==(string)$alias) {

        // Get the most fitting result falling back with alias without language
        $alias = db_result(db_query("SELECT dst FROM {url_alias} WHERE src = '%s' AND language IN('%s', '') ORDER BY language DESC", $path, $path_language));

        // this is a special case to allow memcache to save empties and not confuse them with FALSE
        // store the alias but only if it contains data
        turbo_path_memcache_put('dst', $path, $path_language, $alias, $bin);
        turbo_path_memcache_put('src', $alias, $path_language, $path, $bin);
        // store the source
      }

      $map[$path_language][$path] = $alias;
      return $alias;
    }
    // Check $no_src for this $path in case we've already determined that there
    // isn't a path that has this alias
    elseif ($action == 'source' && !isset($no_src[$path_language][$path])) {
      // Look for the value $path within the cached $map
      $src = '';
      if (!isset($map[$path_language]) || !($src = array_search($path, $map[$path_language]))) {
        // Get the most fitting result falling back with alias without language
        $src = turbo_path_memcache_get('src', $path, $path_language, $bin);

        if ($src!==(string)$src) {

          $src = db_result(db_query("SELECT src FROM {url_alias} WHERE dst = '%s' AND language IN('%s', '') ORDER BY language DESC", $path, $path_language));

          $map[$path_language][$src] = $path;

          turbo_path_memcache_put('src', $path, $path_language, $src, $bin);
          turbo_path_memcache_put('dst', $src, $path_language, $path, $bin);
        }
        else {
          // We can't record anything into $map because we do not have a valid
          // index and there is no need because we have not learned anything
          // about any Drupal path. Thus cache to $no_src.
          $no_src[$path_language][$path] = TRUE;
        }
      }

      return $src;
    }
  }

  return FALSE;
}

/**
* Put item to memceche
**/
function turbo_path_memcache_put($what='src', $src, $language, $data, $bin) {

  $prefix=turbo_path_get_prefix($bin);

  if ($language) {
    $key = $what .'@'. $src .'|'. $language;
  }
  else {
    $key = $what .'@'. $src;
  }

  $data=trim($data);

  if ($data=="") $data="__nothing__";

  $rs = $GLOBALS['turbo_path_memcache'] -> set($prefix . $key, $data, MEMCACHE_COMPRESSED);

  return $rs;
}

/**
* Get item from memcache
**/
function turbo_path_memcache_get($what='src', $src, $language, $bin=NULL) {

  $prefix=turbo_path_get_prefix($bin);

  if ($language) {

    $key = $what .'@'. $src .'|'. $language;

    $rs = $GLOBALS['turbo_path_memcache'] -> get($prefix . $key);

    if ($rs=="__nothing__") return "";

    if ($rs=="") return FALSE;

    return $rs;
  }

  // try without the language
  $key = $what .'@'. $src;

  $rs = $GLOBALS['turbo_path_memcache'] -> get($prefix . $key);

  if ($rs=="__nothing__") return "";

  if ($rs=="") return FALSE;

  return $rs;
}

/**
* Delete item form memcache
**/
function turbo_path_memcache_delete($key, $bin) {
  if (!turbo_path_memcache_connect($bin)) return;
  $prefix=turbo_path_get_prefix($bin);
  $rs = $GLOBALS['turbo_path_memcache'] -> delete($prefix . $key);
}

/**
* Get all items from memcache
**/
function turbo_path_memcache_index_get($bin) {
  $list = '';
  global $language;
  $language = $path_language ? $path_language : $language->language;
  $prefix=turbo_path_get_prefix($bin);
  $res = db_query("SELECT dst,src FROM {url_alias} WHERE language IN('%s', '') ORDER BY language DESC", $language);
  while ($result = db_fetch_array($res) ) {
    $key_src_alias = 'src@'. $result['dst'] .'|'. $language;
    $list[$key_src_alias] = '';
    $key_dst_alias = 'dst@'. $result['src'] .'|'. $language;
    $list[$key_dst_alias] = '';
  }
  return $list;
}

/**
* Get count of all items in memcache
**/
function turbo_path_memcache_count() {
  //TODO: must be aditionally checked
  $status = $GLOBALS['turbo_path_memcache']->getStats();
  print_r($GLOBALS['turbo_path_memcache']);
  $status ["total_items"] = ( !isset($status ["total_items"]) ) ? 1 : $status ["total_items"];
  return $status ["total_items"];
}
