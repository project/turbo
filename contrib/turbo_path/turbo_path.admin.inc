<?php
/**
 * @file
 * provides admin interface to turbo path
 */

define('TURBO_PATH_PATH',   $GLOBALS['conf']['turbo_path']['module_path']);
define('TURBO_PATH_FILTER', variable_get('turbo_path_filter', ''));
define('TURBO_PATH_OPTION', variable_get('turbo_path_option', 0));

/**
* Clear cache work for file engine now
* @return
*   TRUE
**/
function turbo_path_clear_cache() {

  global $language;
  $path_language = $path_language ? $path_language : $language->language;

  foreach ($GLOBALS['conf']['turbo_path']['search_order'] as $bin_id => $bin_name) {

    require_once TURBO_PATH_PATH .'/path.'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'.inc';
    $engine_function = 'turbo_path_'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'_index_get';
    $this_index = $engine_function($GLOBALS['conf']['turbo_path']['bins'][$bin_name]);

    if ( !is_array($this_index) ) continue;

    foreach ($this_index as $key => $value) {
      if ($key=="src@node|". $path_language || $key=="dst@__nothing__|". $path_language) continue;
      $delete_function = 'turbo_path_'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'_delete';
      $rs = $delete_function($key, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);
      watchdog('turbo_path', 'Removing cache from %bin', array('%bin' => $GLOBALS['conf']['turbo_path']['bins'][$bin_name]), 'notice');
    }
  }
  return TRUE;
}

/**
* Dellete item from turbo path cache
* @return
*   TRUE
**/
function turbo_path_delete_key($key, $type=NULL) {
  global $language;
  $path_language = $path_language ? $path_language : $language->language;

  foreach ($GLOBALS['conf']['turbo_path']['search_order'] as $bin_id => $bin_name) {

    require_once TURBO_PATH_PATH .'/path.'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'.inc';

    $delete_function = 'turbo_path_'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'_delete';

    if ( $type=='src' ) {
      $rs = $delete_function("src@" . $key . "|" . $path_language, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);

    }
    elseif ( $type=='dst' ) {
      $rs = $delete_function("dst@" . $key . "|" . $path_language, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);
    }
    else {
      $rs = $delete_function("src@" . $key . "|" . $path_language, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);
      $rs = $delete_function("dst@" . $key . "|" . $path_language, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);
    }
  }
  return TRUE;
}

/**
* Page filter callback
* @return
*   $form
**/
function turbo_path_filter() {

  $form = array();
  $options = array(t('Cache every page except the listed pages.'), t('Cache only the listed pages.'));
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '%blog' for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  if (user_access('use PHP for block visibility')) {
    $options[] = t('Cache pages for which the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
    $description .= t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can severely break your Drupal site.', array('%php' => '<?php ?>'));
  }

  $form['turbo_path_option'] = array(
    '#type'          => 'radios',
    '#title'         => t('Filter specific paths'),
    '#options'       => $options,
    '#default_value' => variable_get('turbo_path_option', 0),
  );

  $form['turbo_path_filter'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths to filter'),
    '#default_value' => variable_get('turbo_path_filter', '
*.png
*.jpeg
*.bmp
*.gif
*.jpg'
    ),
  );

  return system_settings_form($form);
}

/**
* Page index callback
* @return
*   $form
**/
function turbo_path_indexes() {

  $form = array();

  if ( !is_array( $GLOBALS['conf']['turbo_path']['search_order'] ) || count($GLOBALS['conf']['turbo_path']['search_order']) == 0) {
    die("Turbo path isn't configured correctly, follow " . l('INSTALL.txt', drupal_get_path('module', 'turbo_path') . '/INSTALL.txt') . ".");
  }

  foreach ($GLOBALS['conf']['turbo_path']['search_order'] as $bin_id => $bin_name) {

    module_load_include('inc', 'turbo_path', 'path.'. $bin_name);

    $func = 'turbo_path_' . $bin_name . '_count';

    if ( $bin_name == 'file') {
      $form['index_count' . $bin_name] = array(
        '#type' => 'item',
        '#value' => t('Files: Total Turbo Path files: @count.', array('@count' => $func($GLOBALS['conf']['turbo_path']['bins'][$bin_name]['path']))),
      );
    }
    $form['clear_cache'] = array(
      '#type' => 'submit',
      '#value' => t('Clear Path Cache'),
      '#weight' => 100
    );
  }
  return $form;
}

/**
* Index page submit callback
**/
function turbo_path_indexes_submit($form, &$form_state) {
  turbo_path_clear_cache();
  drupal_set_message(t('Turbo Path cache was cleared successfully.'));
}
