TURBO PATH INSTALLATION

1. Apply the provided modules_path.patch against the core path module located in modules/path

2. Add the following snippet to the end of sites/default/settings.php

$conf['turbo_path'] = array(
  'search_order' => array('memcache'), // "memcache" or "file" engine or "memcache" and "file" in any order
  'module_path' => 'sites/all/modules/turbo/contrib/turbo_path', // path to module
  'bins' => array(
    'file' => array(
      'engine' => 'file',
      'prefix' => 'main',
      'path' => 'sites/default/files/turbo_path' // here files will be stored
     ),
    'memcache' => array(
      'engine' => 'memcache',
      'prefix' => 'main',
      'servers' => array('memcacheserver:11211'),
    )
   ),
);

3. Apply the provided includes_path.patch against the core file path.inc.php located in includes

4. Activate the Turbo Path module

