<?php
/**
 * @file
 * synchronizes database paths with those in memcache
 */

define('TURBO_PATH_PATH', $GLOBALS['conf']['turbo_path']['module_path']);

function turbo_path_cron_run() {
  global $language;

  // run this only once per hour
  $last_sync = variable_get('turbo_path_last_cron', 0);

  if (time()-$last_sync<=3600) return;

  $path_language = $path_language ? $path_language : $language->language;

  // create the clean index
  $rs = db_query("SELECT * FROM {url_alias}");
  while ($record = db_fetch_object($rs)) {
    $master_index['dst@'. $record->src .'|'. $path_language]=1;
    $master_index['src@'. $record->dst .'|'. $path_language]=1;
  }

  // get all the indexes
  foreach ($GLOBALS['conf']['turbo_path']['search_order'] as $bin_id => $bin_name) {
    require_once TURBO_PATH_PATH .'/path.'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'.inc';

    $engine_function = 'turbo_path_'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'_index_get';
    $this_index = $engine_function($GLOBALS['conf']['turbo_path']['bins'][$bin_name]);

    foreach ($this_index as $key => $value) {
      if (!$master_index[$key]) {
        if ($key=="src@node|". $path_language || $key=="dst@__nothing__|". $path_language) continue;
        $delete_function = 'turbo_path_'. $GLOBALS['conf']['turbo_path']['bins'][$bin_name]['engine'] .'_delete';
        $rs = $delete_function($key, $GLOBALS['conf']['turbo_path']['bins'][$bin_name]);
      }
    }
  }

  variable_set('turbo_path_last_cron', time());
}
