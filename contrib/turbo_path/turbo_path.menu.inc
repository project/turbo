<?php
/**
 * @file
 * provides menu for turbo path
 */
function turbo_path_menu_run() {
  $items = array();

  $items['admin/settings/performance/turbo'] = array(
   'title' => 'Turbo',
   'description' => 'Provides a performance and scalability boost',
   'access arguments' => array('administer url aliases'),
   'page callback' => 'turbo_path_main',
   'file' => 'turbo_path.menu.inc',
  );

  $items['admin/settings/performance/turbo/path/index'] = array(
   'title' => 'Turbo Path Indexes',
   'description' => 'Shows all indexes',
   'access arguments' => array('administer url aliases'),
   'page callback'  => 'drupal_get_form',
   'page arguments' => array('turbo_path_indexes'),
   'file' => 'turbo_path.admin.inc',
   'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/performance/turbo/path/filter'] = array(
   'title' => 'Turbo Path Filter',
   'description' => 'Create a list of paths to skip',
   'access arguments' => array('administer url aliases'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('turbo_path_filter'),
   'file' => 'turbo_path.admin.inc',
   'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

function turbo_path_main() {
  $text = '';

  if ( !is_array( $GLOBALS['conf']['turbo_path']['search_order'] ) || count($GLOBALS['conf']['turbo_path']['search_order']) == 0) {
    die("Turbo path isn't configured correctly, follow " . l('INSTALL.txt', drupal_get_path('module', 'turbo_path') . '/INSTALL.txt') . ".");
  }

  foreach ($GLOBALS['conf']['turbo_path']['search_order'] as $bin_id => $bin_name) {
    $text .= 'Turbo Path engine: "' . $bin_name . '" <br />';
  }
  return $text;
}