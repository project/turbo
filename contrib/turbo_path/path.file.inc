<?php

/**
 * @file
 * provides flat file caching method
 */

define('TURBO_PATH_FILE_PER_FOLDER', 1000);

function turbo_path_file_lookup_path($action, $path = '', $path_language = '', $bin=NULL) {

  global $language;
  // $map is an array with language keys, holding arrays of Drupal paths to alias relations
  static $map = array(), $no_src = array(), $count;

  $path_language = $path_language ? $path_language : $language->language;

  // Use $count to avoid looking up paths in subsequent calls if there simply are no aliases
  $count = 1;

  if ($action == 'wipe') {
    $map = array();
    $no_src = array();
    $count = NULL;

    for ($index = 0; $index < _dir_max($filepath); $index++) {

      $d = dir($bin['path'] . '/' . $index);

      while (false !== ($entry = $d->read())) {
        if ($entry=='.' || $entry=='..') continue;
        unlink($bin['path'] .'/'. $index . '/' . $entry);
      }
      $d -> close();
    }
  }
  elseif ($count > 0 && $path != '') {

    if ($action == 'alias') {

      if (isset($map[$path_language][$path])) {
        return $map[$path_language][$path];
      }

      $alias = turbo_path_file_get('dst', $path, $path_language, $bin['path'], $bin);

      if ($alias!==(string)$alias) {

        // Get the most fitting result falling back with alias without language
        $alias = db_result(db_query("SELECT dst FROM {url_alias} WHERE src = '%s' AND language IN('%s', '') ORDER BY language DESC", $path, $path_language));

        // store the alias
        turbo_path_file_put('dst', $path, $path_language, $alias, $bin['path'], $bin);
        turbo_path_file_put('src', $alias, $path_language, $path, $bin['path'], $bin);
      }
      $map[$path_language][$path] = $alias;
      return $alias;
    }
    // Check $no_src for this $path in case we've already determined that there
    // isn't a path that has this alias
    elseif ($action == 'source' && !isset($no_src[$path_language][$path])) {
      // Look for the value $path within the cached $map
      $src = '';
      if (!isset($map[$path_language]) || !($src = array_search($path, $map[$path_language]))) {
        // Get the most fitting result falling back with alias without language

        $src = turbo_path_file_get('src', $path, $path_language, $bin['path'], $bin);

        $key1 = 'src'.'@'. $path .'|'. $path_language;
        $key2 = 'src'.'@'. $path;


        if ($src!==(string)$src) {

          $src = db_result(db_query("SELECT src FROM {url_alias} WHERE dst = '%s' AND language IN('%s', '') ORDER BY language DESC", $path, $path_language));

          $map[$path_language][$src] = $path;
          turbo_path_file_put('src', $path, $path_language, $src, $bin['path'], $bin);
          turbo_path_file_put('dst', $src, $path_language, $path, $bin['path'], $bin);
        }
        else {
          // We can't record anything into $map because we do not have a valid
          // index and there is no need because we have not learned anything
          // about any Drupal path. Thus cache to $no_src.
          $no_src[$path_language][$path] = TRUE;
        }
      }
      return $src;
    }
  }

  return FALSE;
}

/**
* Get file content
* @return string
**/
function turbo_path_file_get($what='src', $src, $language, $filepath, $bin=NULL) {

  $prefix = turbo_path_get_prefix($bin);
  // first try it with the language
  if ($language) {
    $key = $what .'@'. $src .'|'. $language;

    $file = urlencode($prefix .':'. $key);

    for ($index = 0; $index <= _dir_count($filepath); $index++) {
      if (file_exists($filepath . '/' . $index . '/' . $file)) {
        $rs = file_get_contents($filepath . '/' . $index . '/' . $file);
        return $rs;
      }
    }
  }

  // try without the language
  $key = $what .'@'. $src;
  $file = urlencode($prefix .':'. $key);

  for ($index = 0; $index <= _dir_max($filepath); $index++) {

    if (file_exists($filepath . '/' . $index . '/' . $file)) {
      $rs = file_get_contents($file);
      return $rs;
    }
  }

  return FALSE;
}

/**
* Create file and dir to store url_alias info
* @return
*   int The function returns the number of bytes that were written to the file, or false on failure.
**/
function turbo_path_file_put($what='src', $src, $language, $data, $filepath, $bin) {
  $prefix = turbo_path_get_prefix($bin);

  $subdir = _dir_max($filepath);

  if (!is_dir($bin['path'])) mkdir($bin['path'], 0777, TRUE);
  if (!is_dir($bin['path'] . '/' . $subdir)) mkdir($bin['path'] . '/' . $subdir, 0777, TRUE);

  // handle a special case
  if ($what=='dst' && $src=='len') return;

  if ($language) {
    $key = $what .'@'. $src .'|'. $language;
  }
  else {
    $key = $what .'@'. $src;
  }


  $file = $filepath .'/'. $subdir . '/' . urlencode($prefix .':'. $key);

  $data = trim($data);
  $rs = file_put_contents($file, $data, LOCK_EX);
  return $rs;
}

/**
* Calculate current file count in cache
* @return
*   int file count
**/
function turbo_path_file_count($filepath) {
  return _count_files($filepath);
}

/**
* Create all file list
* @return
*   array("file_name" => 1)
**/
function turbo_path_file_index_get($bin) {
  $prefix = turbo_path_get_prefix($bin);
  $index = _list_files($bin['path'], $prefix);
  return $index;
}

/**
* Delete file
* @return
*   bool deleted or not
**/
function turbo_path_file_delete($key, $bin) {
  for ($index = 0; $index <= _dir_count($bin['path']); $index++) {
    if (file_exists($bin['path'] .'/'. $index . '/' . $key)) {
      $rs = unlink($bin['path'] .'/'. $index . '/' . $key);
    }
    else {
      $rs = TRUE;
    }
  }

  return $rs;
}

/**
* Count files
* @return
*   int
**/
function _count_files($path) {
  $path .= '/';
  $files = 0;
  $dir = opendir($path);

  if (!$dir) {
    return 0;
  }

  while (($file = readdir($dir)) !== FALSE) {
    if ($file[0] == '.' || $file[0] == '..') {
      continue;
    }

    if (is_dir($path . $file)) {
        $files += _count_files($path . $file);
    }
    else {
        $files++;
    }
  }
  closedir($dir);
  return $files;
}

/**
* List files
* @return
*   array()
**/
function _list_files($path, $prefix, $list = array()) {

  static $list;
  $handle  = opendir($path);
  while (false !== ($file = readdir($handle))) {
    $dir = $path . '/' . $file;

    if (is_dir($dir) && $file != '.' && $file !='..' ) {
      _list_files($dir, $prefix, $list);
    }
    elseif ($file != '.' && $file !='..') {

      list($pref, $key) = explode(":", urldecode($file));

      if ( $prefix == $pref) {
        $list[$file] = 1;
      }
    }
  }
  closedir($handle);
  return $list;
}

/**
* Get current folder name to store files in
* @return
*   int
**/
function _dir_max($path) {
  return floor( turbo_path_file_count($path) / TURBO_PATH_FILE_PER_FOLDER );
}

/**
* Get current folder count to know where to search files
* @return
*   int
**/
function _dir_count($path) {
  $path .= '/';
  $dirs = 0;
  $dir = opendir($path);

  if (!$dir) {
    return 0;
  }

  while (($file = readdir($dir)) !== FALSE) {
    if ($file[0] == '.' || $file[0] == '..') {
      continue;
    }

    if (is_dir($path . $file)) {
        $dirs ++;
    }
  }
  closedir($dir);
  return $dirs;
}